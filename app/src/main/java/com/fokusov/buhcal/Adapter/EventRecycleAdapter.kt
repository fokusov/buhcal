package com.fokusov.buhcal.Adapter

import android.content.Context
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.fokusov.buhcal.DB.EventModule
import com.fokusov.buhcal.Model.Event
import com.fokusov.buhcal.Model.Settings
import com.fokusov.buhcal.R
import org.json.JSONArray
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.R.attr.path




/**
 * Created by Igor on 26.01.2018.
 */
class EventRecycleAdapter (val context : Context, val mySettings: Settings) : RecyclerView.Adapter<EventRecycleAdapter.Holder>() {

    private val EventList: MutableList<Event>
//    private var ind = 1
    private val cal : Calendar = GregorianCalendar()
    private var thisYear = 1970
    private var thisMonth = 1

    init {
        //Log.d("buhcaltest", "adapter init")
        EventList = EventModule.allActiveEvents()
        //Log.d("buhcaltest", "EventList.size: ${EventList.size}")
        if (EventList.size == 0){
            loadDB()
//            Log.d("buhcaltest", "loadDB() executed")
        }
        thisYear = cal.get(Calendar.YEAR)
        thisMonth = cal.get(Calendar.MONTH)
        updateEvents(thisYear, thisMonth, mySettings)
        //Log.d("buhcaltest", "updateEvents() executed")
        //Log.d("buhcaltest", "DB loaded, EventList.size: ${EventList.size}")
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.list_item, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return EventList.size
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bindEvent(EventList[position], context)
    }

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        val eventName = itemView?.findViewById<TextView>(R.id.eventName)
        val dayNumber = itemView?.findViewById<TextView>(R.id.dayNumber)
        val finished = itemView?.findViewById<CheckBox>(R.id.cb_finished)

        fun bindEvent(event: Event, context: Context){
            eventName?.text = event.task
            dayNumber?.text = event.day()
            finished?.isChecked = event.isDone
            if (event.isDone){
                eventName?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                eventName?.paintFlags = Paint.ANTI_ALIAS_FLAG
            }

            finished?.setOnClickListener {
                if (event.isDone){
                    finished.isChecked = false
                    event.isDone = false
                    event.doneDate = Date(0,0,0)
                    event.save()
                    eventName?.paintFlags = Paint.ANTI_ALIAS_FLAG
                } else {
                    finished.isChecked = true
                    event.isDone = true
                    event.doneDate = Date()
                    event.save()
                    eventName?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                }
            }
        }
    }

    fun updateEvents(year: Int, month: Int, pref: Settings){
        thisYear = year
        thisMonth = month
        EventList.clear()
        val evLst = EventModule.listFilteredEvents(year, month)
        evLst.forEach {
            if (checkEvent(it, pref)){
                EventList.add(it)
            }
        }
        notifyDataSetChanged()
    }

    private fun checkEvent(event: Event, pref: Settings): Boolean {
        if (pref.IPUSNRab && event.isIPUSNRab) return true
        if (pref.IPUSN0 && event.isIPUSN0) return true
        if (pref.IPENVDRab && event.isIPENVDRab) return true
        if (pref.IPENVD0 && event.isIPENVD0) return true
        if (pref.IPPat && event.isIPPat) return true
        if (pref.OOOUSN && event.isOOOUSN) return true
        if (pref.OOOENVD && event.isOOOENVD) return true
        if (pref.OOOOSNO && event.isOOOOSNO) return true
        if (pref.ALKO && event.isALKO) return true
        if (pref.ESHN && event.isESHN) return true

        return false
    }

    private fun loadDB(){
        var eventArrayFromJson : ArrayList<JSONArray> = loadJSONFromAsset()
        eventArrayFromJson.forEach {
            for (i in 0..(it.length() - 1)){
                val item = it.getJSONObject(i)
                val dateFormat = SimpleDateFormat("dd.MM.yyyy")
                val dateOfEvent = dateFormat.parse(item["date"].toString())
                var c = GregorianCalendar()
                c.time = dateOfEvent

//                Log.d("buhcaltest", "IN loadDB(): $i")

                val mEvent = Event(item["id"] as Int, item["task"].toString(),
                        dateOfEvent, item["instance"].toString(), item["info"].toString(),
                        false,
                        Date(0,0,0), item["IPUSNRab"] == "+",
                        item["IPUSN0"] == "+", item["IPENVDRab"] == "+",
                        item["IPENVD0"] == "+", item["IPPat"] == "+",
                        item["OOOUSN"] == "+", item["OOOENVD"] == "+",
                        item["OOOOSNO"] == "+", item["ALKO"] == "+",
                        item["ESHN"] == "+")
                mEvent.save()
            }
        }

    }

    private fun loadJSONFromAsset(): ArrayList<JSONArray> {
        var json: String? = null
        val listJsonObj = ArrayList<JSONArray>()
        val fileList = listAssetFiles("")
        if (fileList.isNotEmpty()){
            fileList.forEach {
                Log.d("buhcaltest", it)
                try {
                    json = context.assets.open(it).bufferedReader().use {
                        it.readText()
                    }
                } catch (ex: IOException) {
                    ex.printStackTrace()
                    json = "[]"
                }
                val jsArr = JSONArray(json)
                listJsonObj.add(jsArr)
            }
        }
        return listJsonObj
    }

    private fun listAssetFiles(path: String): ArrayList<String> {
        val fileList = ArrayList<String>()
        val list: Array<String>
        try {
            list = context.assets.list(path)
            if (list.isNotEmpty()) {
                for (file in list) {
                    if (file.contains(".json")) {
                        fileList.add(file)
                    }
                }
            }
        } catch (e: IOException) {
            Log.d("buhcaltest", "Exception in listAssetFiles: $e")
        }
        return fileList
    }

}