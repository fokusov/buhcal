package com.fokusov.buhcal.Controller

import android.app.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.fokusov.buhcal.Adapter.EventRecycleAdapter
import com.fokusov.buhcal.Model.Event
import com.fokusov.buhcal.R
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import com.google.android.gms.ads.MobileAds
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import com.fokusov.buhcal.Model.Settings
import com.fokusov.buhcal.Notification.NotificationHelper
import com.google.gson.Gson


class MainActivity : AppCompatActivity() {

    lateinit var adapter : EventRecycleAdapter
    lateinit var mySettings: Settings
    private val gson = Gson()
    private val cal : Calendar = GregorianCalendar()
    private lateinit var myPref : SharedPreferences
    private var thisYear = 1970
    private var thisMonth = 1
    var isNotifyEnabled = false
    lateinit var mContext : Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mContext = applicationContext
//        sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        myPref = BuhCal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        //ad
        MobileAds.initialize(this, "ca-app-pub-0974678697636006~4283749368")
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        //Settings
        mySettings = Settings(false, false, false, false,
                false, false, false, false, false, false)
        val json = myPref.getString("sel_taxes_json", null)
        if (json != null) mySettings = gson.fromJson(json, Settings::class.java)

        //set calendar
        thisYear = cal.get(Calendar.YEAR)
        thisMonth = cal.get(Calendar.MONTH)
        val initDate = Date(thisYear - 1900, thisMonth, 1)
        val format = SimpleDateFormat("LLLL yyyy")
        monthTextView.text = format.format(initDate)

        isNotifyEnabled = myPref.getBoolean("notify_enabled", false)

        adapter = EventRecycleAdapter(this, mySettings)
        eventRecycleView.adapter = adapter
        eventRecycleView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        val layoutManager = LinearLayoutManager(this)
        eventRecycleView.layoutManager = layoutManager

        prevMonthBtn.setOnClickListener {
            cal.add(Calendar.MONTH, -1)
            adapter.updateEvents(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), mySettings)
            monthTextView.text = format.format(cal.time)
            setVisibility()
        }

        nextMonthBtn.setOnClickListener {
            cal.add(Calendar.MONTH, 1)
            adapter.updateEvents(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), mySettings)
            monthTextView.text = format.format(cal.time)
            setVisibility()
        }

        adapter.updateEvents(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), mySettings)
        setVisibility()

    }

    fun refreshAdapter(){
        val json = myPref.getString("sel_taxes_json", null)
        if (json != null) mySettings = gson.fromJson(json, Settings::class.java)
        adapter.updateEvents(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), mySettings)
        setVisibility()
    }

    fun setVisibility() {

        if (adapter.itemCount == 0) {
            if (settingsIsEmpty()){
                ooops_textview.text = getText(R.string.empty_settings)
            } else {
                ooops_textview.text = getText(R.string.ooops)
            }
            ooops_textview.visibility = View.VISIBLE
            ooops_imageView.visibility = View.VISIBLE
        } else {
            ooops_textview.visibility = View.GONE
            ooops_imageView.visibility = View.GONE
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)
        val menuItem = menu?.findItem(R.id.actions_notify)
        if (isNotifyEnabled) menuItem?.setIcon(R.drawable.ic_notifications_none_white_48dp)
        else menuItem?.setIcon(R.drawable.ic_notifications_off_white_48dp)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_settings -> {
                val modalFragment = ModalFragment().show(supportFragmentManager, "Settings")
                return true
            }
//            R.id.action_set_time -> {
//                val cal2 = Calendar.getInstance()
//
//                val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
//                    cal2.set(Calendar.HOUR_OF_DAY, hour)
//                    cal2.set(Calendar.MINUTE, minute)
//                    sharedPref.edit().putInt("notify_hour", hour).commit()
//                    sharedPref.edit().putInt("notify_minute", minute).commit()
//                    Toast.makeText(this, "Время уведомления установлено на " +
//                            "${SimpleDateFormat("HH:mm").format(cal2.time)}",
//                            Toast.LENGTH_SHORT).show()
//                }
//                TimePickerDialog(this, timeSetListener, sharedPref.getInt("notify_hour", 9),
//                        sharedPref.getInt("notify_minute", 5), true).show()
//
//                return true
//            }
            R.id.actions_notify -> {
                isNotifyEnabled = !isNotifyEnabled
                myPref.edit().putBoolean("notify_enabled", isNotifyEnabled).commit()
                this.supportInvalidateOptionsMenu()
                if (isNotifyEnabled){
                    Toast.makeText(this, "Уведомления включены", Toast.LENGTH_SHORT).show()
                    NotificationHelper().scheduleNotificationOfEvents(mContext)
                    NotificationHelper().enableBootReceiver(mContext)
                } else {
                    Toast.makeText(this, "Уведомления выключены", Toast.LENGTH_SHORT).show()
                    NotificationHelper().cancelAlarmRTC()
                    NotificationHelper().disableBootReceiver(mContext)
                }
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun settingsIsEmpty(): Boolean {

        if (!mySettings.IPUSNRab && !mySettings.IPUSN0 && !mySettings.IPENVDRab &&
                !mySettings.IPENVD0 && !mySettings.IPPat && !mySettings.OOOUSN &&
                !mySettings.OOOENVD && !mySettings.OOOOSNO && !mySettings.ALKO &&
                !mySettings.ESHN) return true

        return false
    }

}
