package com.fokusov.buhcal.Controller

import android.app.Application
import android.content.Context
import com.fokusov.buhcal.Model.Event
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.config.GeneratedDatabaseHolder

/**
 * Created by Igor on 05.02.2018.
 */
class BuhCal : Application() {
    companion object {
        var ctx: Context? = null
    }
    override fun onCreate() {
        super.onCreate()
        FlowManager.init(FlowConfig.Builder(this).openDatabasesOnInit(true).build())
        ctx = applicationContext
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
    }
}