package com.fokusov.buhcal.Controller


import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fokusov.buhcal.Model.Settings

import com.fokusov.buhcal.R
import com.google.gson.Gson


/**
 * A simple [Fragment] subclass.
 */
class ModalFragment : DialogFragment() {

    private lateinit var myPref : SharedPreferences
    lateinit var mySettings : Settings

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val textView = TextView(activity)
        textView.setText(R.string.filter)
        return textView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val gson = Gson()
        val builder = AlertDialog.Builder(activity)
        //sharedPref = activity.getPreferences(Context.MODE_PRIVATE)
        myPref = BuhCal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)

        mySettings = Settings(false, false, false, false,
                false, false, false, false, false,
                false)
        val json = myPref.getString("sel_taxes_json", null)
        if (json != null) mySettings = gson.fromJson(json, Settings::class.java)
        val checkedArray = mySettings.checkedItems()

        builder.setTitle(R.string.filter)
                .setMultiChoiceItems(R.array.taxes_system, checkedArray,
                    DialogInterface.OnMultiChoiceClickListener { dialog, which, isChecked ->
                        if (isChecked) {
                            checkItems(which, true)
                        } else {
                            checkItems(which, false)
                        }
                    })
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener {
                    dialog, id ->
                    run {
                        myPref.edit().putString("sel_taxes_json", gson.toJson(mySettings)).commit()
                        (activity as MainActivity).refreshAdapter()
                    }
                })
                .setNegativeButton(R.string.cancel, DialogInterface.OnClickListener {
                    dialog, id -> })

        return builder.create()
    }

    private fun checkItems(mInd : Int, trueFalse : Boolean) {
        when (mInd) {
            0 -> mySettings.IPUSNRab = trueFalse
            1 -> mySettings.IPUSN0 = trueFalse
            2 -> mySettings.IPENVDRab = trueFalse
            3 -> mySettings.IPENVD0 = trueFalse
            4 -> mySettings.IPPat = trueFalse
            5 -> mySettings.OOOUSN = trueFalse
            6 -> mySettings.OOOENVD = trueFalse
            7 -> mySettings.OOOOSNO = trueFalse
            8 -> mySettings.ALKO = trueFalse
            9 -> mySettings.ESHN = trueFalse
        }
    }

}// Required empty public constructor
