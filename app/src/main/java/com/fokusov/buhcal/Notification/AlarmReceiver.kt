package com.fokusov.buhcal.Notification

import com.fokusov.buhcal.R
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.app.PendingIntent
import android.support.v7.app.NotificationCompat
import android.util.Log
import com.fokusov.buhcal.Controller.MainActivity
import java.util.*


/**
 * Created by Igor on 03.02.2018.
 */
class AlarmReceiver : BroadcastReceiver() {
    var evName = ""
    var evId = 1

    override fun onReceive(context: Context?, intent: Intent?) {

        if (intent != null) {
            evName = intent.getStringExtra("evName")
            evId   = intent.getIntExtra("evId", 1)
        }

        //Intent to invoke app when click on notification.
        //In this sample, we want to start/launch this sample app when user clicks on notification
        val intentToRepeat = Intent(context, MainActivity::class.java)

        //set flag to restart/relaunch the app
        intentToRepeat.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        //Pending intent to handle launch of Activity in intent above
        val pendingIntent = PendingIntent.getActivity(context, evId, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT)

        //Build notification
        val myNotification = buildLocalNotification(context, pendingIntent).build()

        //Send local notification
        NotificationHelper().getNotificationManager(context).notify(evId, myNotification)
    }

    fun buildLocalNotification(context: Context?, pendingIntent: PendingIntent): NotificationCompat.Builder {

        return NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_notifications_none_white_48dp)
                .setContentTitle("Календарь бухгалтера")
                .setContentText(evName)
                .setAutoCancel(true) as NotificationCompat.Builder
    }
}