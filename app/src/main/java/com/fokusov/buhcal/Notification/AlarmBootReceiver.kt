package com.fokusov.buhcal.Notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created by Igor on 03.02.2018.
 */
class AlarmBootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent != null) {
            if (intent.action == "android.intent.action.BOOT_COMPLETED" ||
                    intent.action == "android.intent.action.QUICKBOOT_POWERON" ||
                    intent.action == "android.intent.action.REBOOT") {
                if (context != null) {
                    Log.d("buhcaltest", "scheduling on BOOT")
                    NotificationHelper().scheduleNotificationOfEvents(context)
                } else {
                    Log.d("buhcaltest", "context is null !!!")
                }
            }
        }
    }

}