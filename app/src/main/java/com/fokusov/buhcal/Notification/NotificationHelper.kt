package com.fokusov.buhcal.Notification

import android.app.PendingIntent
import android.app.AlarmManager
import android.content.pm.PackageManager
import android.content.ComponentName
import android.content.Context
import android.app.NotificationManager
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import java.util.*
import android.os.SystemClock
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import com.fokusov.buhcal.Controller.BuhCal
import com.fokusov.buhcal.DB.EventModule
import com.fokusov.buhcal.Model.Event
import com.fokusov.buhcal.Model.Settings
import com.google.gson.Gson


/**
 * Created by Igor on 03.02.2018.
 */
class NotificationHelper() : Parcelable {
    var ALARM_TYPE_RTC = 100
    private var alarmManagerRTC: AlarmManager? = null
    private var alarmIntentRTC: PendingIntent? = null

    var ALARM_TYPE_ELAPSED = 101
    private var alarmManagerElapsed: AlarmManager? = null
    private var alarmIntentElapsed: PendingIntent? = null

    private lateinit var myPref : SharedPreferences
    lateinit var mySettings : Settings
    private val cal : Calendar = GregorianCalendar()

    constructor(parcel: Parcel) : this() {
        ALARM_TYPE_RTC = parcel.readInt()
        alarmIntentRTC = parcel.readParcelable(PendingIntent::class.java.classLoader)
        ALARM_TYPE_ELAPSED = parcel.readInt()
        alarmIntentElapsed = parcel.readParcelable(PendingIntent::class.java.classLoader)
    }

    fun scheduleNotificationOfEvents(context: Context) {
        //prefs
        val gson = Gson()
        myPref = BuhCal.ctx!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        mySettings = Settings(false, false, false, false,
                false, false, false, false, false,
                false)
        val json = myPref.getString("sel_taxes_json", null)
        if (json != null) mySettings = gson.fromJson(json, Settings::class.java)
        //
        val evLst = EventModule.listNewEvents(Date())
        evLst.forEach {
            if (checkEvent(it, mySettings)) {
//                Log.d("buhcaltest", "scheduling of ${it.date}")
                cal.time = it.date

                cal.set(Calendar.HOUR, 9)
                val intent = Intent(context, AlarmReceiver::class.java)
                intent.putExtra("evName", it.task)
                intent.putExtra("evId", it.id)

                alarmIntentRTC = PendingIntent.getBroadcast(context, it.id, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT)
                alarmManagerRTC = context.getSystemService(ALARM_SERVICE) as AlarmManager
                alarmManagerRTC!!.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, alarmIntentRTC)
            }
        }
    }

    private fun checkEvent(event: Event, pref: Settings): Boolean {

        if (pref.IPUSNRab && event.isIPUSNRab) return true
        if (pref.IPUSN0 && event.isIPUSN0) return true
        if (pref.IPENVDRab && event.isIPENVDRab) return true
        if (pref.IPENVD0 && event.isIPENVD0) return true
        if (pref.IPPat && event.isIPPat) return true
        if (pref.OOOUSN && event.isOOOUSN) return true
        if (pref.OOOENVD && event.isOOOENVD) return true
        if (pref.OOOOSNO && event.isOOOOSNO) return true
        if (pref.ALKO && event.isALKO) return true
        if (pref.ESHN && event.isESHN) return true

        return false
    }

    fun scheduleRepeatingRTCNotification(context: Context, hour: String, min: String) {
        //get calendar instance to be able to select what time notification should be scheduled
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        //Setting time of the day (8am here) when notification will be sent every day (default)
        calendar.set(Calendar.HOUR_OF_DAY,
                Integer.getInteger(hour, 9),
                Integer.getInteger(min, 0))

        //Setting intent to class where Alarm broadcast message will be handled
        val intent = Intent(context, AlarmReceiver::class.java)
        //Setting alarm pending intent
        alarmIntentRTC = PendingIntent.getBroadcast(context, ALARM_TYPE_RTC, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        //getting instance of AlarmManager service
        alarmManagerRTC = context.getSystemService(ALARM_SERVICE) as AlarmManager

        //Setting alarm to wake up device every day for clock time.
        //AlarmManager.RTC_WAKEUP is responsible to wake up device for sure, which may not be good practice all the time.
        // Use this when you know what you're doing.
        //Use RTC when you don't need to wake up device, but want to deliver the notification whenever device is woke-up
        //We'll be using RTC.WAKEUP for demo purpose only
        alarmManagerRTC!!.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntentRTC)
    }

    fun scheduleRepeatingElapsedNotification(context: Context) {
        //Setting intent to class where notification will be handled
        val intent = Intent(context, AlarmReceiver::class.java)

        //Setting pending intent to respond to broadcast sent by AlarmManager everyday at 8am
        alarmIntentElapsed = PendingIntent.getBroadcast(context, ALARM_TYPE_ELAPSED, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        //getting instance of AlarmManager service
        alarmManagerElapsed = context.getSystemService(ALARM_SERVICE) as AlarmManager

        //Inexact alarm everyday since device is booted up. This is a better choice and
        //scales well when device time settings/locale is changed
        //We're setting alarm to fire notification after 15 minutes, and every 15 minutes there on
        alarmManagerElapsed!!.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntentElapsed)
    }

    fun cancelAlarmRTC() {
        if (alarmManagerRTC != null) {
            alarmManagerRTC!!.cancel(alarmIntentRTC)
        }
    }

    fun cancelAlarmElapsed() {
        if (alarmManagerElapsed != null) {
            alarmManagerElapsed!!.cancel(alarmIntentElapsed)
        }
    }

    fun getNotificationManager(context: Context?): NotificationManager {
        return context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    /**
     * Enable boot receiver to persist alarms set for notifications across device reboots
     */
    fun enableBootReceiver(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.getPackageManager()

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)
    }

    /**
     * Disable boot receiver when user cancels/opt-out from notifications
     */
    fun disableBootReceiver(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.getPackageManager()

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ALARM_TYPE_RTC)
        parcel.writeParcelable(alarmIntentRTC, flags)
        parcel.writeInt(ALARM_TYPE_ELAPSED)
        parcel.writeParcelable(alarmIntentElapsed, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationHelper> {
        override fun createFromParcel(parcel: Parcel): NotificationHelper {
            return NotificationHelper(parcel)
        }

        override fun newArray(size: Int): Array<NotificationHelper?> {
            return arrayOfNulls(size)
        }
    }

}