package com.fokusov.buhcal.DB

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by Igor on 08.02.2018.
 */
@Database(version = BuhCalDataBase.VERSION, name = BuhCalDataBase.NAME)
object BuhCalDataBase {
    const val VERSION = 1
    const val NAME = "buhcal"
}