package com.fokusov.buhcal.DB

import com.fokusov.buhcal.Model.Event
import com.fokusov.buhcal.Model.Event_Table
import com.raizlabs.android.dbflow.kotlinextensions.list
import com.raizlabs.android.dbflow.sql.language.Select
import java.util.*


/**
 * Created by Igor on 08.02.2018.
 */
object EventModule {

    fun allActiveEvents(): MutableList<Event>{
        return Select().from(Event::class.java).list
    }

    fun findEvent(id: Int): Event? {
        return Select().from(Event::class.java).where(Event_Table.id.eq(id)).querySingle()
    }

    fun listFilteredEvents(year: Int, month: Int): MutableList<Event>{
        val monthStart = getDate(month, year, true)
        val monthEnd = getDate(month, year, false)

        return Select().from(Event::class.java)
                .where(Event_Table.date.between(monthStart).and(monthEnd))
                .queryList()
    }

    fun listNewEvents(dt: Date): MutableList<Event>{
        return Select().from(Event::class.java)
                .where(Event_Table.date.greaterThan(dt))
                .queryList()
    }

    private fun getDate(month: Int, year: Int, start: Boolean): Date {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, 1)
        if (start) {
            calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE))
        } else {
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE))
        }
        return calendar.time
    }
}