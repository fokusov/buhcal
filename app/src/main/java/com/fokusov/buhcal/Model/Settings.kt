package com.fokusov.buhcal.Model


/**
 * Created by Igor on 26.01.2018.
 */
class Settings(var IPUSNRab: Boolean, var IPUSN0: Boolean, var IPENVDRab: Boolean,
               var IPENVD0: Boolean, var IPPat: Boolean, var OOOUSN: Boolean,
               var OOOENVD: Boolean, var OOOOSNO: Boolean, var ALKO: Boolean, var ESHN: Boolean) {

    fun checkedItems() : BooleanArray {
        val mSelectedTaxes = arrayListOf(false, false, false, false, false, false, false, false, false, false)
        if (IPUSNRab) mSelectedTaxes[0] = true
        if (IPUSN0) mSelectedTaxes[1] = true
        if (IPENVDRab) mSelectedTaxes[2] = true
        if (IPENVD0) mSelectedTaxes[3] = true
        if (IPPat) mSelectedTaxes[4] = true
        if (OOOUSN) mSelectedTaxes[5] = true
        if (OOOENVD) mSelectedTaxes[6] = true
        if (OOOOSNO) mSelectedTaxes[7] = true
        if (ALKO) mSelectedTaxes[8] = true
        if (ESHN) mSelectedTaxes[9] = true

        return mSelectedTaxes.toBooleanArray()
    }
}