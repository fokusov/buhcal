package com.fokusov.buhcal.Model

import com.fokusov.buhcal.DB.BuhCalDataBase
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import com.raizlabs.android.dbflow.structure.BaseModel
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Igor on 26.01.2018.
 * Описание события, включая кастомные события
 */
@Table(name = "Events", database = BuhCalDataBase::class, allFields = true)
class Event (@PrimaryKey var id : Int = 0,
             var task : String = "",
             var date : Date = Date(0, 0, 0),
             var instance : String = "",
             var description : String = "",
             var isDone : Boolean = false,
             var doneDate : Date = Date(0,0,0),
             var isIPUSNRab: Boolean = false,
             var isIPUSN0: Boolean = false,
             var isIPENVDRab: Boolean = false,
             var isIPENVD0: Boolean = false,
             var isIPPat: Boolean = false,
             var isOOOUSN: Boolean = false,
             var isOOOENVD: Boolean = false,
             var isOOOOSNO: Boolean = false,
             var isALKO: Boolean = false,
             var isESHN: Boolean = false) : BaseModel() {

    override fun toString(): String {
        return task
    }

    fun day() : String {
        val format = SimpleDateFormat("dd")
        return format.format(date)
    }

    fun dayOfWeek() : String {
        val format = SimpleDateFormat("E")
        return format.format(date)
    }

}